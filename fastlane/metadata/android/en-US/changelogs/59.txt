REL_59 (2024-10-18)
------------------
* Bugfix release
* Implement work round for CertificateException error in REST calls
* Release EFIS v6.19
