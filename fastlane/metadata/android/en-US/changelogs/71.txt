REL_71 (2025-01-09)
------------------
* Maintenance release
* Only allow fat finger for spinner/waypoint entry
* Improvements to traffic display
* Add dedicated TCAS/Traffic screen
* Release EFIS v7.01
