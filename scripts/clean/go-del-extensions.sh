#!/bin/bash


#  <?xml version="1.0" encoding="UTF-8"?>
#  <osm version="0.6" generator="CGImap 0.0.2">
#      <bounds minlat="53.2875000" minlon="-6.2274000" maxlat="53.3046000" maxlon="-6.1941000"/>
#      <node id="389719" lat="53.3067754" lon="-6.2129946" user="Jazzmax" uid="110583" visible="true" version="2" changeset="21774" timestamp="2009-04-01T03:08:52Z"/>
#      <node id="19223905" lat="53.2954887" lon="-6.2035759" user="mackerski" uid="6367" visible="true" version="1" changeset="397382" timestamp="2007-09-08T23:45:14Z">
#          <tag k="created_by" v="JOSM"/>
#      </node>
#      <node id="19223906" lat="53.2941060" lon="-6.2025008" user="Nick Burrett" uid="166593" visible="true" version="5" changeset="6041772" timestamp="2010-10-14T18:29:52Z">
#           <tag k="highway" v="traffic_signals"/>
#      </node>
#  </osm>


#xmlns="http://www.topografix.com/GPX/1/1"

#xmlstarlet ed -N xmlns="http://www.topografix.com/GPX/1/1" --delete "/gpx/wpt/extensions" airport.gpx.xml > output.xml
# xmlstarlet ed -N x="http://www.topografix.com/GPX/1/1" --delete "/gpx/wpt/extensions" airport.gpx.xml 

grep -v navaid airport.gpx.xml >~a.xml
xmlstarlet ed --delete "/gpx/wpt/extensions" ~a.xml  > output.xml
rm ./~a.xml

# Or just use grep for all, but xml at least formats nice.
