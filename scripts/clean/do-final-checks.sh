#!/bin/bash


APP_ID=player.efis.pfd 
#APP_ID=player.efis.data.eur.rus.txt
#APP_ID=player.efis.data.usa.can.txt
#APP_ID=player.efis.data.zar.aus.txt

#See if fdroid readmeta runs without any errors. If it
../fdroidserver/fdroid readmeta

#Run fdroid rewritemeta app.id to clean up your file
../fdroidserver/fdroid rewritemeta $APP_ID

#Run fdroid checkupdates app.id to fill automated fields like Auto Name and Current Version
../fdroidserver/fdroid checkupdates $APP_ID

#Make sure that fdroid lint app.id doesn't report any warnings. If it does, fix them.
../fdroidserver/fdroid lint $APP_ID

#Test your build recipe with fdroid build -v -l app.id
../fdroidserver/fdroid build -v -l $APP_ID