#!/bin/bash

#pskill java
#pskill adb
adb devices

cd ./apk

if [ ! -z $1 ] && [ $1 == '-nw' ] 
then
    echo 'wifi nexus 7'
	DROIDDEVICE=015d2ea4a467ec11
fi

if [ ! -z $1 ] && [ $1 == '-ng' ] 
then
    echo 'gsm nexus 7'
	DROIDDEVICE=015d3249295c160b
fi

if [ ! -z $1 ] && [ $1 == '-a20' ] 
then
    echo 'samsung a20'
	DROIDDEVICE=RF8R20WQ8AB
fi

if [ ! -z $1 ] && [ $1 == '-a5' ] 
then
    echo 'samsung a5'
	DROIDDEVICE=521049d5ec7044ef
fi

if [ ! -z $1 ] && [ $1 == '-bv' ] 
then
    echo 'blackview bv6000s'
	DROIDDEVICE=CQAA5L8S599DSWDQ
fi

if [ ! -z $1 ] && [ $1 == '-t3' ] 
then
    echo 'Samsung TAB3'
	DROIDDEVICE=4790243370453100
fi

if [ ! -z $1 ]
then
    adb devices

    #wifi nexus 7
    adb -s 015d2ea4a467ec11 install -r ./kwik-efis.apk
    #gsm nexus 7
    adb -s 015d3249295c160b install -r ./kwik-efis.apk
    #samsung a20
    adb -s RF8R20WQ8AB      install -r ./kwik-efis.apk
    #samsung a5
    adb -s 521049d5ec7044ef install -r ./kwik-efis.apk
    #samsung s5
    adb -s 758f9cc3         install -r ./kwik-efis.apk
    #blackview bv6000s	
    adb -s CQAA5L8S599DSWDQ install -r ./kwik-efis.apk
    #samsung TAB3
    adb -s 4790243370453100 install -r ./kwik-efis.apk



    #adb -s $DROIDDEVICE install -r ./kwik-efis.apk


    if [ "$2" == "all" ]
    then 
        adb -s $DROIDDEVICE uninstall player.efis.data.zar.aus
        adb -s $DROIDDEVICE uninstall player.efis.data.usa.can
        adb -s $DROIDDEVICE uninstall player.efis.data.eur.rus
        adb -s $DROIDDEVICE uninstall player.efis.data.sah.jap
        adb -s $DROIDDEVICE uninstall player.efis.data.pan.arg
        adb -s $DROIDDEVICE uninstall player.efis.data.ant.spl

        adb -s $DROIDDEVICE install -r ./kwik-efis-datapac-zar.aus.apk
        adb -s $DROIDDEVICE install -r ./kwik-efis-datapac-usa.can.apk
        adb -s $DROIDDEVICE install -r ./kwik-efis-datapac-eur.rus.apk
        adb -s $DROIDDEVICE install -r ./kwik-efis-datapac-sah.jap.apk
        adb -s $DROIDDEVICE install -r ./kwik-efis-datapac-pan.arg.apk
        adb -s $DROIDDEVICE install -r ./kwik-efis-datapac-ant.spl.apk
    fi
else 
    # adb uninstall player.efis.pfd
    adb install -r ./kwik-efis.apk
fi



#pskill java
#pskill adb
