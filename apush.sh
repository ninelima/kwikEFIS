#!/bin/bash 

# export SSHPASS=password

# rw-r--r--
# 0644

. ./zarniwoop123.sh
sshpass -e sftp -oBatchMode=no -b - dh_d4rhb9@zarniwoop.dreamhost.com << !
    cd ninelima.org/efis
    put  ./apk/kwik-efis.apk 
    put  ./apk/CHANGELOG.md 
    put  ./webpage/efis/download.html 
    bye 
!

if [ "$1" == "all" ]
then 
  . ./zarniwoop123.sh
  sshpass -e sftp -oBatchMode=no -b - dh_d4rhb9@zarniwoop.dreamhost.com << !
      cd ninelima.org/efis
      put  ./apk/kwik-efis-datapac-ant.spl.apk
      put  ./apk/kwik-efis-datapac-eur.rus.apk
      put  ./apk/kwik-efis-datapac-pan.arg.apk
      put  ./apk/kwik-efis-datapac-sah.jap.apk
      put  ./apk/kwik-efis-datapac-usa.can.apk
      put  ./apk/kwik-efis-datapac-zar.aus.apk
      bye 
!
fi

cp  ./apk/kwik-efis.apk /cygdrive/v/Sync/Public/KwikEFIS
#cp  ./apk/kwik-dmap.apk /cygdrive/v/Sync/Public/KwikEFIS
#cp  ./apk/kwik-comp.apk /cygdrive/v/Sync/Public/KwikEFIS
cp  ./apk/CHANGELOG.md /cygdrive/v/Sync/Public/KwikEFIS
