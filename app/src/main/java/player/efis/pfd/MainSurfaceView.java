/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package player.efis.pfd;

import android.content.Context;

//
// A view container where OpenGL ES graphics can be drawn on screen.
// This view can also be used to capture touch events, such as a user
// interacting with drawn objects.
//
public class MainSurfaceView extends EFISSurfaceView
{
    //public final CFDRenderer mRenderer;  // normally this would be private but we want to access the sel wpt from main activity
    //public CFDRenderer mRenderer;  // normally this would be private but we want to access the sel wpt from main activity

    public MainSurfaceView(Context context)
    {
        super(context);

        ///*
        // Create an OpenGL ES 2.0 context.
        setEGLContextClientVersion(2);

        // Set the Renderer for drawing on the GLSurfaceView
        //-- mRenderer = new CFDRenderer(context); // = new MyGLRenderer();  --b2
        //-- setRenderer(mRenderer);

        // Render the view only when there is a change in the drawing data
        //-- setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
        //*/

        /*
        Then just use this Renderer in your GLSurfaceView

        glSurfaceView.setEGLContextClientVersion(1);
        glSurfaceView.setRenderer(new BitmapRenderer(getResources()));
        glSurfaceView.setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
        */
        /*
        mRenderer = new CFDRenderer(context); // = new MyGLRenderer();  --b2
        setEGLContextClientVersion(1);  //1
        setRenderer(new BitmapRenderer(getResources()));
        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
        //*/

        /*
        // Create an OpenGL ES 2.0 context.
        setEGLContextClientVersion(2);  //2

        // Set the Renderer for drawing on the GLSurfaceView
        mRenderer = new CFDRenderer(context);
        setRenderer(mRenderer);

        // Render the view only when there is a change in the drawing data
        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
        //*/

    }
}
